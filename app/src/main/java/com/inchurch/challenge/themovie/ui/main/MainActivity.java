package com.inchurch.challenge.themovie.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.ui.bookmark.BookmarkActivity;
import com.inchurch.challenge.themovie.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bookmark_nav_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bookmark:
                goToBookmarks();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void goToBookmarks() {
        startActivity(new Intent(this, BookmarkActivity.class));
    }
}
