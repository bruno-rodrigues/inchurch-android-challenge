package com.inchurch.challenge.themovie.persistence.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.inchurch.challenge.themovie.domain.Genre;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface GenreDao {

    @Query("SELECT * FROM genre")
    List<Genre> getAllGenres();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ArrayList<Genre> genres);
}
