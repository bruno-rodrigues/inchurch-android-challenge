package com.inchurch.challenge.themovie.api;

import com.inchurch.challenge.themovie.domain.GenreResponse;
import com.inchurch.challenge.themovie.domain.PopularResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TMDBWebservice {
    String BASE_URL = "https://api.themoviedb.org";
    String BASE_IMAGES_URL = "https://image.tmdb.org/t/p/w500/";
    String API_KEY = "f4c4de61d569b553bd375c25aef8db01";

    @GET("/3/movie/popular?language=en-US")
    Call<PopularResponse> getPopular(@Query("api_key") String apiKey, @Query("page") Integer page);

    @GET("/3/genre/movie/list?language=en-US")
    Call<GenreResponse> getGenres(@Query("api_key") String apiKey);
}
