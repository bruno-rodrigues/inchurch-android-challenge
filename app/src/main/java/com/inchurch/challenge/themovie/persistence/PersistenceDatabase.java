package com.inchurch.challenge.themovie.persistence;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.inchurch.challenge.themovie.domain.Genre;
import com.inchurch.challenge.themovie.domain.Movie;
import com.inchurch.challenge.themovie.persistence.converters.Converters;
import com.inchurch.challenge.themovie.persistence.dao.GenreDao;
import com.inchurch.challenge.themovie.persistence.dao.MovieDao;

@Database(entities = {Movie.class, Genre.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class PersistenceDatabase extends RoomDatabase {
    private static PersistenceDatabase INSTANCE;
    private static final String DB_NAME = "themovie.db";

    public static PersistenceDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (PersistenceDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PersistenceDatabase.class, DB_NAME)
                            .allowMainThreadQueries()
                            .addCallback(new RoomDatabase.Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Log.d("PersistenceDatabase", "Database created.");
                                }
                            })
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    public abstract MovieDao movieDao();

    public abstract GenreDao genreDao();
}
