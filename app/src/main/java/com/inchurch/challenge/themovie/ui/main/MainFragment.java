package com.inchurch.challenge.themovie.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.inchurch.challenge.themovie.ui.detail.DetailActivity;
import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.domain.Movie;

public class MainFragment extends Fragment implements MainRecyclerViewAdapter.ItemClickListener {

    private MainViewModel viewModel;
    private MainRecyclerViewAdapter adapter;

    private ProgressBar progressBar;
    private ProgressBar mainProgressBar;

    private boolean isLoading = false;
    private Integer page;

    public static MainFragment newInstance() {
         return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        page = 1;
        progressBar = getView().findViewById(R.id.progress_bar);
        mainProgressBar = getView().findViewById(R.id.main_progress_bar);

        RecyclerView recyclerView = getView().findViewById(R.id.main_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getMovies().getValue().size() - 1) {
                        progressBar.setVisibility(View.VISIBLE);
                        viewModel.loadPopularMovies(getContext(), page);
                        isLoading = true;

                    }
                }
            }
        });
        adapter = new MainRecyclerViewAdapter(getContext());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.getMovies().observe(getViewLifecycleOwner(), movies -> {
            if(movies.size() > 0) {
                adapter.setMovies(movies);
                page++;
                progressBar.setVisibility(View.GONE);
                isLoading = false;

                if(mainProgressBar.getVisibility() != View.GONE) {
                    mainProgressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.loadPopularMovies(getContext(), page);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        Movie movie = viewModel.getMovies().getValue().get(position);
        String jsonMovie = new Gson().toJson(movie);
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra("movie", jsonMovie);
        startActivity(intent);
    }
}
