package com.inchurch.challenge.themovie.persistence.converters;

import androidx.room.TypeConverter;

import java.util.Arrays;
import java.util.Date;

public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static int[] fromString(String value) {
        String[] strings = value.replace("[", "").replace("]", "").split(", ");
        int result[] = new int[strings.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(strings[i]);
        }
        return result;
    }

    @TypeConverter
    public static String intArrayToString(int[] arr) {
        return arr == null ? null : Arrays.toString(arr);
    }
}
