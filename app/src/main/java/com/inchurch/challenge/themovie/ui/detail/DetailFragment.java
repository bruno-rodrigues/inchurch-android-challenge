package com.inchurch.challenge.themovie.ui.detail;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.api.TMDBWebservice;
import com.inchurch.challenge.themovie.domain.Movie;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DetailFragment extends Fragment {

    private DetailViewModel viewModel;

    public static DetailFragment newInstance(Bundle bundle) {
        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ImageView backdropImage = getView().findViewById(R.id.backdrop_image);
        TextView title = getView().findViewById(R.id.title_text);
        TextView released = getView().findViewById(R.id.release_date_text);
        TextView genre = getView().findViewById(R.id.genre_text);
        TextView overview = getView().findViewById(R.id.overview_text);

        viewModel = ViewModelProviders.of(getActivity()).get(DetailViewModel.class);
        viewModel.loadGenres(getContext());
        viewModel.getMovie().observe(getViewLifecycleOwner(), movie -> {
            String url = TMDBWebservice.BASE_IMAGES_URL + movie.backdropPath;
            Picasso.get().load(url).into(backdropImage);

            title.setText(movie.title);
            if(movie.releaseDate != null) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String releasedText = getResources().getString(R.string.release_date) + df.format(movie.releaseDate);
                released.setText(releasedText);
            }
            overview.setText(movie.overview);
            String genreText = viewModel.getGenreStringByIds(movie.genreIds);
            genre.setText(genreText);
        });

        Bundle bundle = getArguments();
        String jsonMovie = bundle.getString("movie");
        Movie movie = new Gson().fromJson(jsonMovie, Movie.class);

        viewModel.setMovie(movie);
    }
}
