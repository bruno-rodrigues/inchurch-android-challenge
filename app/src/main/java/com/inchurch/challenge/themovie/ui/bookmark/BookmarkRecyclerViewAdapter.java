package com.inchurch.challenge.themovie.ui.bookmark;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.api.TMDBWebservice;
import com.inchurch.challenge.themovie.domain.Movie;
import com.inchurch.challenge.themovie.persistence.PersistenceDatabase;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class BookmarkRecyclerViewAdapter extends RecyclerView.Adapter<BookmarkRecyclerViewAdapter.ViewHolder> implements Filterable {

    private ArrayList<Movie> data;
    private ArrayList<Movie> filteredData;
    private LayoutInflater inflater;
    private BookmarkRecyclerViewAdapter.ItemClickListener clickListener;
    private Context context;

    public BookmarkRecyclerViewAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
        filteredData = new ArrayList<>();
    }

    @NonNull
    @Override
    public BookmarkRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.bookmark_card_item, parent, false);
        return new BookmarkRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookmarkRecyclerViewAdapter.ViewHolder holder, int position) {
        String url = TMDBWebservice.BASE_IMAGES_URL + filteredData.get(position).posterPath;
        Picasso.get().load(url).into(holder.posterImage);
        holder.titleText.setText(filteredData.get(position).title);
        holder.overviewText.setText(filteredData.get(position).overview);
        holder.deleteBookmark.setOnClickListener(v -> {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.delete_bookmark_title_alert)
                    .setMessage(R.string.delete_bookmark_message_alert)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        PersistenceDatabase.getInstance(context).movieDao().deleteById(filteredData.get(position).id);
                        if (clickListener != null) clickListener.onItemRemoved(position);
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        });

        if(filteredData.get(position).releaseDate != null) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String releasedText = df.format(filteredData.get(position).releaseDate);
            holder.releasedText.setText(releasedText);
        }
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredData.clear();
                    filteredData.addAll(data);
                } else {
                    ArrayList<Movie> tempFiltered = new ArrayList<>();
                    for (Movie row : data) {
                        if (row.title.toLowerCase().contains(charString.toLowerCase()) || row.overview.contains(charString)) {
                            tempFiltered.add(row);
                        }
                    }

                    filteredData = tempFiltered;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (clickListener != null) clickListener.onDataSetChanged(filteredData.size());
                notifyDataSetChanged();
            }
        };
    }

    public void setMovies(ArrayList<Movie> movies) {
        data = movies;
        filteredData = new ArrayList<>();
        filteredData.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titleText;
        public TextView releasedText;
        public TextView overviewText;
        public ImageView posterImage;
        public ImageButton deleteBookmark;

        ViewHolder(View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.title_text);
            releasedText = itemView.findViewById(R.id.released_text);
            overviewText = itemView.findViewById(R.id.overview_text);
            posterImage = itemView.findViewById(R.id.poster_image);
            deleteBookmark = itemView.findViewById(R.id.delete_bookmark);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(getAdapterPosition());
        }
    }

    public void setClickListener(BookmarkRecyclerViewAdapter.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int position);
        void onItemRemoved(int position);
        void onDataSetChanged(int dataLength);
    }
}
