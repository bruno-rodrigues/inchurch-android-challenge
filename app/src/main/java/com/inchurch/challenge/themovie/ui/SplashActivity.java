package com.inchurch.challenge.themovie.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.api.TMDBRepository;
import com.inchurch.challenge.themovie.domain.GenreResponse;
import com.inchurch.challenge.themovie.persistence.PersistenceDatabase;
import com.inchurch.challenge.themovie.ui.main.MainActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TMDBRepository repository = new TMDBRepository();
        repository.getGenres(new TMDBRepository.RepositoryCallback<GenreResponse>() {
            @Override
            public void onResponse(GenreResponse result) {
                PersistenceDatabase.getInstance(getApplicationContext()).genreDao().insert(result.genres);
                SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }

            @Override
            public void onFailure() {
                Toast.makeText(getApplicationContext(), R.string.error_message_toast, Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
}
