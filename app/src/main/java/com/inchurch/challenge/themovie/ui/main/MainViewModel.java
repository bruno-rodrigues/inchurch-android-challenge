package com.inchurch.challenge.themovie.ui.main;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.api.TMDBRepository;
import com.inchurch.challenge.themovie.domain.Movie;
import com.inchurch.challenge.themovie.domain.PopularResponse;

import java.util.ArrayList;

public class MainViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Movie>> movies;

    public MainViewModel() {
        movies = new MutableLiveData<>();
    }

    public void loadPopularMovies(Context context, Integer page) {
        TMDBRepository repository = new TMDBRepository();
        repository.getPopular(page, new TMDBRepository.RepositoryCallback<PopularResponse>() {
            @Override
            public void onResponse(PopularResponse result) {
                ArrayList<Movie> temp = movies.getValue();
                if(temp == null) temp = new ArrayList<>();
                temp.addAll(result.results);
                movies.setValue(temp);
            }

            @Override
            public void onFailure() {
                Toast.makeText(context, R.string.error_message_toast, Toast.LENGTH_LONG).show();
            }
        });
    }

    public MutableLiveData<ArrayList<Movie>> getMovies() {
        return movies;
    }
}
