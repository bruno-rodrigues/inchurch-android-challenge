package com.inchurch.challenge.themovie.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inchurch.challenge.themovie.domain.GenreResponse;
import com.inchurch.challenge.themovie.domain.PopularResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TMDBRepository {
    private Retrofit retrofit;
    private TMDBWebservice webservice;

    public TMDBRepository() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(TMDBWebservice.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        webservice = retrofit.create(TMDBWebservice.class);
    }

    public <T> void getPopular(Integer page, RepositoryCallback<T> repositoryCallback ) {
        webservice.getPopular(TMDBWebservice.API_KEY , page).enqueue(new Callback<PopularResponse>() {
            @Override
            public void onResponse(Call<PopularResponse> call, Response<PopularResponse> response) {
                if(response.isSuccessful()) {
                    if(repositoryCallback != null) repositoryCallback.onResponse((T) response.body());
                } else {
                    if(repositoryCallback != null) repositoryCallback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<PopularResponse> call, Throwable t) {
                if(repositoryCallback != null) repositoryCallback.onFailure();
            }
        });
    }

    public <T> void getGenres(RepositoryCallback<T> repositoryCallback) {
        webservice.getGenres(TMDBWebservice.API_KEY).enqueue(new Callback<GenreResponse>() {
            @Override
            public void onResponse(Call<GenreResponse> call, Response<GenreResponse> response) {
                if(response.isSuccessful()) {
                    if(repositoryCallback != null) repositoryCallback.onResponse((T) response.body());
                } else {
                    if(repositoryCallback != null) repositoryCallback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<GenreResponse> call, Throwable t) {
                if(repositoryCallback != null) repositoryCallback.onFailure();
            }
        });
    }

    public interface RepositoryCallback <T> {
        void onResponse(T result);
        void onFailure();
    }
}
