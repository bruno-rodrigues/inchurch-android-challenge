package com.inchurch.challenge.themovie.domain;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity(tableName = "movie")
public class Movie {

    @PrimaryKey()
    @ColumnInfo(name = "did")
    public Integer id;

    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    public String posterPath;

    @ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    public String backdropPath;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "overview")
    public String overview;

    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    public Date releaseDate;

    @ColumnInfo(name = "genre_ids")
    @SerializedName("genre_ids")
    public int[] genreIds;
}
