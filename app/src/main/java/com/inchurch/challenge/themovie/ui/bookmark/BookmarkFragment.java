package com.inchurch.challenge.themovie.ui.bookmark;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.inchurch.challenge.themovie.ui.detail.DetailActivity;
import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.domain.Movie;

public class BookmarkFragment extends Fragment implements BookmarkRecyclerViewAdapter.ItemClickListener {

    private BookmarkViewModel viewModel;
    private  BookmarkRecyclerViewAdapter adapter;

    private LinearLayout emptyState;
    private TextView emptyStateText;

    public static BookmarkFragment newInstance() {
        return new BookmarkFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bookmark_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        emptyState = getView().findViewById(R.id.empty_state);
        emptyStateText = getView().findViewById(R.id.empty_state_text);

        viewModel = ViewModelProviders.of(getActivity()).get(BookmarkViewModel.class);

        RecyclerView recyclerView = getView().findViewById(R.id.bookmark_recyler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BookmarkRecyclerViewAdapter(getContext());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        viewModel.getMovies().observe(getViewLifecycleOwner(), movies -> {
            adapter.setMovies(movies);
            configureEmptyState(movies.size(), false);
        });

        viewModel.getQuery().observe(getViewLifecycleOwner(), s -> {
            adapter.getFilter().filter(s);
        });
    }

    private void configureEmptyState(int dataLength, Boolean isSearching) {
        if(dataLength > 0) {
            emptyState.setVisibility(View.GONE);
        } else {
            emptyStateText.setText(isSearching ? R.string.empty_bookmark_search_message : R.string.empty_bookmark_list_message);
            emptyState.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.loadBookmarkMovies(getActivity());
    }

    @Override
    public void onItemClick(int position) {
        Movie movie = viewModel.getMovies().getValue().get(position);
        String jsonMovie = new Gson().toJson(movie);
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra("movie", jsonMovie);
        startActivity(intent);
    }

    @Override
    public void onItemRemoved(int position) {
        viewModel.loadBookmarkMovies(getActivity());
    }

    @Override
    public void onDataSetChanged(int dataLength) {
        configureEmptyState(dataLength, true);
    }
}
