package com.inchurch.challenge.themovie;

import com.inchurch.challenge.themovie.persistence.converters.Converters;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ConvertersTest {
    @Test
    public void testFromString() {
        int[] RESULT = {1, 2, 3, 4};
        assertArrayEquals("Conversion from string represent an array into array of int failed", RESULT, Converters.fromString("[1, 2, 3, 4]"));
    }

    @Test
    public void testIntArrayToString() {
        int[] ACT = {1, 2, 3, 4};
        String EXPECTED = "[1, 2, 3, 4]";
        assertEquals("Conversion from array int into string failed", EXPECTED, Converters.intArrayToString(ACT));
    }

    @Test
    public void testFromTimestamp() {
        Long timestamp = 1587655936L;
        Date EXPECTED = new Date(timestamp);
        assertEquals("Conversion from timestamp into Date failed", EXPECTED, Converters.fromTimestamp(timestamp));
    }

    @Test
    public void testDateToTimestamp() {
        Long EXPECTED = 1587655936L;
        Date date = new Date(EXPECTED);
        assertEquals("Conversion from Date into timestamp failed", EXPECTED, Converters.dateToTimestamp(date));
    }
}
