# inChurch Recruitment Process 2020 - Android Developer

Para a arquitetura do projeto foi utilizado um modelo proximo do MVVM, mantendo um desacoplamento das views com as regras de repositorio local e api. Neste modelo a implementa��o de testes sobre as regras de negocio localizado nos viewModels seria simples.

Para comunica��o com a api da TMDB foi utilizado a biblioteca Retrofit2. foi escolhida gra�as a atualiza��es constantes, vasta documenta��o e uma grande comunidade. Sua implementa��o foi feita apartir da cria��o de um singleton (TMDBRepository) para que uma futura troca da biblioteca n�o gere um grande impacto no c�digo.

J� para persistencia dos dados localmente foi utilizado o Room. Desenvolvido e mantido pelo Google tem uma documenta��o bem completa.

Para mostrar os generos dos filmes uma chamada a parte � feita para buscar todos os generos disponiveis e assim montar o texto. Para evitar chamadar desnecessarias esse endpoint � consultado e persistido durante a inicializa��o do aplicativo e consultado quando necess�rio.

Pela falta de tempo n�o foi possivel fazer a implementa��o de todos os testes necess�rios.

## Alguma d�vida?

* E-mail: brunoemmanuel@gmail.com
