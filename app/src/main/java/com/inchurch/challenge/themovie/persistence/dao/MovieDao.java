package com.inchurch.challenge.themovie.persistence.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.inchurch.challenge.themovie.domain.Movie;

import java.util.List;

@Dao
public interface MovieDao {

    @Query("SELECT * FROM movie ORDER BY title ASC")
    List<Movie> getAllMovies();

    @Query("DELETE FROM movie WHERE did = :id")
    void deleteById(Integer id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Movie movie);

    @Query("SELECT * FROM movie WHERE did = :id LIMIT 1")
    Movie findMovieById(Integer id);
}
