package com.inchurch.challenge.themovie.ui.bookmark;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.inchurch.challenge.themovie.domain.Movie;
import com.inchurch.challenge.themovie.persistence.PersistenceDatabase;

import java.util.ArrayList;
import java.util.List;

public class BookmarkViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Movie>> movies;
    private MutableLiveData<String> query;

    public BookmarkViewModel() {
        movies = new MutableLiveData<>();
        query = new MutableLiveData<>();
    }

    public void loadBookmarkMovies(Context context) {
        List<Movie> persistedMovies = PersistenceDatabase.getInstance(context).movieDao().getAllMovies();
        movies.setValue((ArrayList<Movie>) persistedMovies);
    }

    public LiveData<ArrayList<Movie>> getMovies() {
        return movies;
    }

    public LiveData<String> getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query.setValue(query);
    }
}
