package com.inchurch.challenge.themovie.ui.detail;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.inchurch.challenge.themovie.domain.Genre;
import com.inchurch.challenge.themovie.domain.Movie;
import com.inchurch.challenge.themovie.persistence.PersistenceDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailViewModel extends ViewModel {
    private MutableLiveData<Movie> movie;

    private Map<Integer, String> genreMap;

    public DetailViewModel() {
        movie = new MutableLiveData<>();
        genreMap = new HashMap<>();
    }

    public void loadGenres(Context context) {
        List<Genre> persistedGenres = PersistenceDatabase.getInstance(context).genreDao().getAllGenres();
        for (Genre genre: persistedGenres) {
            genreMap.put(genre.id, genre.name);
        }
    }

    public LiveData<Movie> getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie.setValue(movie);
    }

    public String getGenreStringByIds(int[] ids) {
        String result = "";
        for (int i = 0; i < ids.length; i++) {
            Integer id = ids[i];
            String genre = genreMap.get(id);

            if(genre == null) continue;

            result = result + genre;
            if(i + 1 < ids.length) {
                result = result + ", ";
            }
        }

        return result;
    }

    public Boolean isPersisted(Context context) {
        Movie movie = PersistenceDatabase.getInstance(context).movieDao().findMovieById(this.movie.getValue().id);
        return (movie != null);
    }

    public void addToBookmark(Context context) {
        if(isPersisted(context)) {
            PersistenceDatabase.getInstance(context).movieDao().deleteById(movie.getValue().id);
        } else {
            PersistenceDatabase.getInstance(context).movieDao().insert(movie.getValue());
        }
    }
}
