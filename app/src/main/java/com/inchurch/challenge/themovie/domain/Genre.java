package com.inchurch.challenge.themovie.domain;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "genre")
public class Genre {

    @PrimaryKey()
    @ColumnInfo(name = "did")
    public Integer id;

    @ColumnInfo(name = "name")
    public String name;
}
