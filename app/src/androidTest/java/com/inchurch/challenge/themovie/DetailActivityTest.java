package com.inchurch.challenge.themovie;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.inchurch.challenge.themovie.ui.detail.DetailActivity;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DetailActivityTest {
    private String MOCK_JSON_MOVIE = "{\"backdrop_path\":\"/5BwqwxMEjeFtdknRV792Svo0K1v.jpg\",\"genre_ids\":[18,878],\"id\":419704,\"overview\":\"Overview text mock.\",\"poster_path\":\"/xBHvZcjRiWyobQ9kxBhO6B2dtRI.jpg\",\"release_date\":\"Sep 17, 2019 12:00:00 AM\",\"title\":\"Ad Astra\"}";

    @Rule
    public ActivityTestRule<DetailActivity> rule = new ActivityTestRule<DetailActivity>(DetailActivity.class) {

        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("movie", MOCK_JSON_MOVIE);
            return intent;
        }

    };

    @Test
    public void correctTitleText() {
        DetailActivity activity = rule.getActivity();

        View viewById = activity.findViewById(R.id.title_text);
        assertThat(viewById, notNullValue());
        assertThat(viewById, instanceOf(TextView.class));
        TextView textView = (TextView) viewById;

        assertThat(textView.getText().toString(), is("Ad Astra"));
    }

    @Test
    public void correctReleasedDateText() {
        DetailActivity activity = rule.getActivity();

        View viewById = activity.findViewById(R.id.release_date_text);
        assertThat(viewById, notNullValue());
        assertThat(viewById, instanceOf(TextView.class));
        TextView textView = (TextView) viewById;

        assertThat(textView.getText().toString(), is("Release date:17/09/2019"));
    }

    @Test
    public void correctOverviewText() {
        DetailActivity activity = rule.getActivity();

        View viewById = activity.findViewById(R.id.overview_text);
        assertThat(viewById, notNullValue());
        assertThat(viewById, instanceOf(TextView.class));
        TextView textView = (TextView) viewById;

        assertThat(textView.getText().toString(), is("Overview text mock."));
    }

    @Test
    public void correctGenreText() {
        DetailActivity activity = rule.getActivity();

        View viewById = activity.findViewById(R.id.genre_text);
        assertThat(viewById, notNullValue());
        assertThat(viewById, instanceOf(TextView.class));
        TextView textView = (TextView) viewById;

        assertThat(textView.getText().toString(), is("Drama, Science Fiction"));
    }
}
