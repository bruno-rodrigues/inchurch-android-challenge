package com.inchurch.challenge.themovie.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.api.TMDBWebservice;
import com.inchurch.challenge.themovie.domain.Movie;
import com.inchurch.challenge.themovie.persistence.PersistenceDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Movie> data;
    private LayoutInflater inflater;
    private ItemClickListener clickListener;
    private Context context;

    public MainRecyclerViewAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.main_card_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String url = TMDBWebservice.BASE_IMAGES_URL + data.get(position).posterPath;
        Picasso.get().load(url).into(holder.posterImage);
        holder.titleText.setText(data.get(position).title);
        Movie movie = PersistenceDatabase.getInstance(context).movieDao().findMovieById(data.get(position).id);
        holder.bookmarkStatus.setVisibility((movie == null) ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setMovies(ArrayList<Movie> movies) {
        data = movies;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titleText;
        public ImageView posterImage;
        public ImageView bookmarkStatus;

        ViewHolder(View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.title_text);
            posterImage = itemView.findViewById(R.id.poster_image);
            bookmarkStatus = itemView.findViewById(R.id.bookmark_status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }
}
