package com.inchurch.challenge.themovie.domain;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PopularResponse {

    @SerializedName("total_results")
    public Integer totalResult;

    public Integer page;

    @SerializedName("total_pages")
    public Integer totalPages;

    public ArrayList<Movie> results;
}
