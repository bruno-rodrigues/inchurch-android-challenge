package com.inchurch.challenge.themovie.ui.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.inchurch.challenge.themovie.R;
import com.inchurch.challenge.themovie.ui.detail.DetailFragment;
import com.inchurch.challenge.themovie.ui.detail.DetailViewModel;

public class DetailActivity extends AppCompatActivity {

    private DetailViewModel detailViewModel;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        detailViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);

        getSupportActionBar().setTitle(R.string.detail_screen_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DetailFragment.newInstance(getIntent().getExtras()))
                    .commitNow();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.bookmark_nav_menu, menu);
        changeIcon();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.bookmark:
                addOrRemoveBookmark(item);
                return true;
            default:
                onBackPressed();
                return super.onOptionsItemSelected(item);
        }

    }

    public void changeIcon(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (menu != null) {
                    MenuItem item = menu.findItem(R.id.bookmark);
                    if (item != null) {
                        Boolean bool = detailViewModel.isPersisted(getApplicationContext());
                        item.setIcon(bool ? android.R.drawable.btn_star_big_on : android.R.drawable.btn_star);
                    }
                }
            }
        });
    }

    private void addOrRemoveBookmark(MenuItem item) {
        detailViewModel.addToBookmark(getApplicationContext());
        changeIcon();
    }
}
